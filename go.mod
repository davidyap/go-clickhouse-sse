module myapp

go 1.15

require (
	github.com/ClickHouse/clickhouse-go v1.4.5
	github.com/david-yappeter/go-indodax v0.0.0-20210604020900-534a4d20a496
	github.com/gorilla/mux v1.8.0
	github.com/iancoleman/strcase v0.1.3
	github.com/stoewer/go-strcase v1.2.0
)
