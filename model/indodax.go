package model

type DBIndodaxData struct {
	Data       string `json:"data"`
	ServerTime int    `json:"server_time"`
}
