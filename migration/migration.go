package migration

import (
	"log"
	"myapp/config"
)

func MigrateTable() {
	db := config.ConnectClickhouse()
	defer db.Close()

	var tables []string = []string{
		`CREATE TABLE IF NOT EXISTS ticker_all 
        (
            data String,
            server_time UInt64
        ) Engine=Memory
        `,
		`CREATE LIVE VIEW IF NOT EXISTS lv_ticker_all AS SELECT * FROM ticker_all ORDER BY server_time desc LIMIT 1`,
		`CREATE TABLE IF NOT EXISTS summaries
        (
            data String,
            server_time UInt64
        ) Engine=Memory`,
		`CREATE LIVE VIEW IF NOT EXISTS lv_summaries AS SELECT * FROM summaries ORDER BY server_time desc LIMIT 1`,
		`CREATE TABLE IF NOT EXISTS price_increments
        (
            data String,
            created_at UInt64    
        ) Engine=Memory`,
		`CREATE LIVE VIEW IF NOT EXISTS lv_price_increments AS SELECT * FROM price_increments ORDER BY created_at desc LIMIT 1`,
	}

	for _, val := range tables {
		if _, err := db.Exec(val); err != nil {
			log.Fatal(err)
		}
	}
}
