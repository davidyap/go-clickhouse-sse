package sse

import (
	"fmt"
	"myapp/config"
	"myapp/model"
	"net/http"
)

type PriceIncrementsBroker struct {
	Notifier       chan []byte
	newClients     chan chan []byte
	closingClients chan chan []byte
	clients        map[chan []byte]bool
}

func NewPriceIncrementsBroker() (broker *PriceIncrementsBroker) {
	broker = &PriceIncrementsBroker{
		Notifier:       make(chan []byte),
		newClients:     make(chan chan []byte),
		closingClients: make(chan chan []byte),
		clients:        make(map[chan []byte]bool),
	}

	go broker.listen()
	go broker.listenLiveData()

	return
}

func (broker *PriceIncrementsBroker) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	flusher, ok := w.(http.Flusher)

	if !ok {
		http.Error(w, "Streaming Unsupported!", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	messageChan := make(chan []byte)

	broker.newClients <- messageChan

	defer func() {
		broker.closingClients <- messageChan
	}()

	notify := r.Context().Done()

	go func() {
		<-notify
		broker.closingClients <- messageChan
	}()

	for {
		fmt.Fprintf(w, "data: %s\n\n", <-messageChan)

		flusher.Flush()
	}
}

func (broker *PriceIncrementsBroker) listen() {
	for {
		select {
		case s := <-broker.newClients:
			broker.clients[s] = true
		case s := <-broker.closingClients:
			delete(broker.clients, s)
		case event := <-broker.Notifier:
			for client := range broker.clients {
				client <- event
			}
		}
	}
}

func (broker *PriceIncrementsBroker) listenLiveData() {
	db := config.ConnectClickhouse()
	defer db.Close()

	rows, err := db.Query("WATCH lv_price_increments")

	if err != nil {
		fmt.Println(err)
		return
	}

	for rows.Next() {
		var dbModel model.DBIndodaxData
		var tempVersion = struct {
			Version int
		}{}

		err := rows.Scan(&dbModel.Data, &dbModel.ServerTime, &tempVersion.Version)
		if err != nil {
			fmt.Println(err)
			return
		}

		// Example Logging
		// fmt.Printf("Ticker All Live Watch: %+v\n", dbModel)

		broker.Notifier <- []byte(dbModel.Data)
	}

}
