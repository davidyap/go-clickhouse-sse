package sse

import (
	"log"
	"myapp/service"
	"time"
)

type MainBroker struct {
	ServerTime      *ServerTimeBroker
	Pairs           *PairsTimeBroker
	PriceIncrements *PriceIncrementsBroker
	TickerAll       *TickerAllBroker
	Summaries       *SummariesBroker
}

func NewBroker() (broker *MainBroker) {
	broker = &MainBroker{
		ServerTime:      NewServerTimeBroker(),
		Pairs:           NewPairsTimeBroker(),
		PriceIncrements: NewPriceIncrementsBroker(),
		TickerAll:       NewTickerAllBroker(),
		Summaries:       NewSummariesBroker(),
	}

	go func() {
		for {
			time.Sleep(time.Second * 5)
			eventByte, err := service.ServerTime()
			if err != nil {
				log.Println("Receiving ERROR event")
				broker.ServerTime.Notifier <- []byte(`{"error": "internal server error"}`)
			} else {
				broker.ServerTime.Notifier <- eventByte
			}
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 5)
			eventByte, err := service.Pairs()
			if err != nil {
				log.Println("Receiving ERROR event")
				broker.Pairs.Notifier <- []byte(`{"error": "internal server error"}`)
			} else {
				broker.Pairs.Notifier <- eventByte
			}
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 5)
			_, err := service.PriceIncrements()
			if err != nil {
				log.Println("Receiving ERROR event")
				broker.PriceIncrements.Notifier <- []byte(`{"error": "internal server error"}`)
			}
			//  else {
			// 	broker.PriceIncrements.Notifier <- eventByte
			// }
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 5)
			_, err := service.TickerAll()
			if err != nil {
				log.Println("Receiving ERROR event")
				broker.TickerAll.Notifier <- []byte(`{"error": "internal server error"}`)
			}
			// else {
			// 	broker.TickerAll.Notifier <- eventByte
			// }
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 5)
			_, err := service.Summaries()
			if err != nil {
				log.Println("Receiving ERROR event")
				broker.Summaries.Notifier <- []byte(`{"error": "internal server error"}`)
			}
			// else {
			// 	broker.Summaries.Notifier <- eventByte
			// }
		}
	}()

	return
}
