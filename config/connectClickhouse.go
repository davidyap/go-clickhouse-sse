package config

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/ClickHouse/clickhouse-go"
)

func ConnectClickhouse() *sql.DB {
	var (
		host      = os.Getenv("DB_HOST")
		port      = os.Getenv("DB_PORT")
		database  = os.Getenv("DB_DATABASE")
		debugMode = os.Getenv("DB_DEBUG_MODE")
	)

	connect, err := sql.Open("clickhouse", fmt.Sprintf("tcp://%s:%s?debug=%s&database=%s", host, port, debugMode, database))
	if err != nil {
		log.Fatal(err)
	}
	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			log.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		} else {
			log.Fatal(err)
		}
		return nil
	}
	connect.Exec("SET allow_experimental_live_view = 1")

	return connect
}
