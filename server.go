package main

import (
	"log"
	"myapp/migration"
	"myapp/sse"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func main() {
	migration.MigrateTable()

	// return

	// go func() {
	// 	db := config.ConnectClickhouse()

	// 	rows, err := db.Query("WATCH lv_ticker")
	// 	if err != nil {
	// 		panic(err)
	// 	}

	// 	for rows.Next() {
	// 		var temp model.Ticker

	// 		rows.Scan(&temp.High, &temp.Low, &temp.VolBtc, &temp.VolIdr, &temp.Last, &temp.Buy, &temp.Sell, &temp.ServerTime, &temp.Version)

	// 		fmt.Printf("RESULT: %+v\n", temp)
	// 	}
	// }()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	mainBroker := sse.NewBroker()

	router := mux.NewRouter()

	router.HandleFunc("/server-time", mainBroker.ServerTime.ServeHTTP)
	router.HandleFunc("/pairs", mainBroker.Pairs.ServeHTTP)
	router.HandleFunc("/price-increments", mainBroker.PriceIncrements.ServeHTTP)
	router.HandleFunc("/ticker-all", mainBroker.TickerAll.ServeHTTP)
	router.HandleFunc("/summaries", mainBroker.Summaries.ServeHTTP)

	log.Println(http.ListenAndServe(":"+port, router))
}
