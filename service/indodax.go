package service

import (
	"encoding/json"
	"myapp/model"
	"time"

	indodax "github.com/david-yappeter/go-indodax"
)

func ServerTime() ([]byte, error) {
	client := indodax.NewDefaultClient()

	resp, err := client.ServerTime()
	if err != nil {
		return nil, err
	}

	jsoned, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}

	return jsoned, nil
}

func Pairs() ([]byte, error) {
	client := indodax.NewDefaultClient()

	resp, err := client.Pairs()
	if err != nil {
		return nil, err
	}

	jsoned, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}

	return jsoned, nil
}

func PriceIncrements() ([]byte, error) {
	client := indodax.NewDefaultClient()

	resp, err := client.PriceIncrements()
	if err != nil {
		return nil, err
	}

	modelJson, err := json.Marshal(resp.Increments)
	if err != nil {
		return nil, err
	}

	PriceIncrementsCreate(model.DBIndodaxData{
		Data:       string(modelJson),
		ServerTime: int(time.Now().UTC().Unix()),
	})

	jsoned, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}

	return jsoned, nil
}

func TickerAll() ([]byte, error) {
	client := indodax.NewDefaultClient()

	resp, err := client.TickerAll()
	if err != nil {
		return nil, err
	}

	modelJson, err := json.Marshal(resp.Tickers)
	if err != nil {
		return nil, err
	}

	TickerAllCreate(model.DBIndodaxData{
		Data:       string(modelJson),
		ServerTime: resp.Tickers.BtcIdr.ServerTime,
	})

	jsoned, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}

	return jsoned, nil
}

func Summaries() ([]byte, error) {
	client := indodax.NewDefaultClient()

	resp, err := client.Summaries()
	if err != nil {
		return nil, err
	}

	modelJson, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}

	SummariesCreate(model.DBIndodaxData{
		Data:       string(modelJson),
		ServerTime: resp.Tickers.BtcIdr.ServerTime,
	})

	jsoned, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}

	return jsoned, nil
}
