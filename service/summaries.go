package service

import (
	"log"
	"myapp/config"
	"myapp/model"
)

func SummariesCreate(input model.DBIndodaxData) {
	db := config.ConnectClickhouse()
	defer db.Close()

	query := "INSERT INTO summaries VALUES (?,?)"

	tx, _ := db.Begin()
	stmt, _ := tx.Prepare(query)
	defer stmt.Close()

	if _, err := stmt.Exec(input.Data, input.ServerTime); err != nil {
		log.Fatal(err)
	}

	if err := tx.Commit(); err != nil {
		log.Fatal(err)
	}
}
